#!/bin/sh

external_dir="$PWD/external"

[ -z "${ROVER_WS}" ] && echo "You must have ROVER_WS set to the rover repository" && exit

# Replace the external folder with the new one with the F410 stuff as well as the L476
rm -rf "$ROVER_WS/tools/firmware/gcc4mbed/external"
mv "./external" "$ROVER_WS/tools/firmware/gcc4mbed/external"

echo "Libraries copied!"
